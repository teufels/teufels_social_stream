<?php
namespace TEUFELS\TeufelsSocialStream\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * SocialStreamController
 */
class SocialStreamController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * socialStreamRepository
     *
     * @var \TEUFELS\TeufelsSocialStream\Domain\Repository\SocialStreamRepository
     * @inject
     */
    protected $socialStreamRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        /*
         * get page id of this page
         */
        $iCurrentPageUid = intval($GLOBALS['TSFE']->id);

        /*
         * get sys_language_uid of this page
         */
        $iSysLanguageUid = intval($GLOBALS['TSFE']->sys_language_uid);

        /*
         * Try to get cached detail view
         *
         * Therefor:
         * calculate the identifier for the cached entry
         */
        $sCacheIdentifier = 'tx_teufelssocialstream_domain_model_socialstream_list_' .
            $iCurrentPageUid . '_' .
            $iSysLanguageUid;


        $sContent = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')
            ->getCache($this->settings['cacheKey'])
            ->get($sCacheIdentifier);

        /*
         * If $sContent is null, it hasn't been cached.
         * => Calculate the value and store it in the cache:
         */
        if ($sContent === FALSE) {
            /*
             * Content not in cache
                 * => generate a cache entry
                 */
            $sContent = $this->listActionContent();
            \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')
                ->getCache($this->settings['cacheKey'])
                ->set(
                    $sCacheIdentifier,
                    $sContent,
                    array($sCacheIdentifier),
                    intval($this->settings['cachePeriod'])
                );
        }

        return $sContent;

    }
    
    public function getFacebookFeeds()
    {
        $fbId = $this->settings['iFacebookId'];
        $fbType = $this->settings['sFacebookStreamType'];
        $fbToken = $this->settings['sFacebookAccessToken'];
        $fbMaxFeeds = intval($this->settings['streamlimit']);
        $streamGetFeedFactor = intval($this->settings['iStreamGetFeedFactor']);
        $fbFeedLimit = $fbMaxFeeds * $streamGetFeedFactor;
        switch ($this->settings['formattoshow']) {
            case 'text_only':    $fields = 'message,link';
                break;
            case 'image_only':    $fields = 'type,picture,full_picture,link';
                break;
            default:    $fields = 'type,message,picture,full_picture,link';
                break;
        }
        $sUrl = 'https://graph.facebook.com/' . $fbId . '/' . $fbType . '?limit=' . $fbFeedLimit . (string) '&' . 'fields=' . $fields . (string) '&' . 'access_token=' . $fbToken;
        /*
         * Create GuzzleHttp\Client
         */
        
        $client = new Client();
        try {    $response = $client->request('GET', $sUrl);
        } catch (RequestException $e) {    $aBody = array(
                'bError' => 1,
                'sDescription' => array(
                    'message' => 'Ooops! ' . $e->getMessage(),
                    'code' => 'Request Exeption'
                )
            );
            $this->view->assign('value', $aBody);
            return $this->view->render();
        }
        $iStatusCode = $response->getStatusCode();
        if ($iStatusCode == 200) {
            /*
             * Generate cache entry
             */
            
            $jBody = $response->getBody();
            $aBody = json_decode($jBody, true);
            $i = 0;
            $aFacebookFeeds = array();
            foreach ($aBody['data'] as $feed) {
                if ($feed['message'] != '' || $feed['picture'] != '') {
                    if ($feed['message'] != '' && $feed['picture'] != '') {
                        $messagetype = 'pictureAndMessage';
                    } elseif ($feed['message'] != '') {
                        $messagetype = 'onlyText';
                    } elseif ($feed['picture'] != '') {
                        $messagetype = 'onlyPicture';
                    }

                    if ($feed['full_picture'] != '') {
                        $imageSize = getimagesize($feed['full_picture']);
                    }

                    $aFacebookFeed = array(
                        'channel' => 'facebook',
                        'timestamp' => strtotime($feed['created_time']),
                        'type' => $feed['type'],
                        'message' => nl2br($feed['message']),
                        'picture' => $feed['picture'],
                        'full_picture' => $feed['full_picture'],
                        'imageWidth' => $imageSize[0],
                        'imageHeight' => $imageSize[1],
                        'link' => $feed['link'],
                        'messagetype' => $messagetype
                    );
                    switch ($this->settings['formattoshow']) {
                        case 'text_only':    if ($feed['message'] != '') {
                            $aFacebookFeeds[strtotime($feed['created_time'])] = $aFacebookFeed;
                            $i++;
                        }
                            break;
                        case 'image_only':    if ($feed['picture'] != '') {
                            $aFacebookFeeds[strtotime($feed['created_time'])] = $aFacebookFeed;
                            $i++;
                        }
                            break;
                        case 'text_with_image':    if ($feed['message'] != '' && $feed['picture'] != '') {
                            $aFacebookFeeds[strtotime($feed['created_time'])] = $aFacebookFeed;
                            $i++;
                        }
                            break;
                        case 'text_and_image':    if ($feed['message'] != '' || $feed['picture'] != '') {
                            $aFacebookFeeds[strtotime($feed['created_time'])] = $aFacebookFeed;
                            $i++;
                        }
                            break;
                    }
                    if ($i >= $fbMaxFeeds) {
                        break;
                    }
                }
            }
        } else {
            $aBody = array(
                'bError' => 1,
                'sDescription' => array(
                    'message' => 'statusCode ' . $iStatusCode,
                    'code' => $iStatusCode
                )
            );
            $this->view->assign('value', $aBody);
            return $this->view->render();
        }
        return $aFacebookFeeds;
    }

    private function listActionContent()
    {
        $streamLimit = intval($this->settings['streamlimit']);
        $fbactive = intval($this->settings['bFacebookActive']);
        $aStreamFeeds = array();
        if ($fbactive === 1) {
            $aFacebookFeeds = $this->getFacebookFeeds();
            $aStreamFeeds = array_merge($aStreamFeeds, $aFacebookFeeds);
        }
        ksort($aStreamFeeds);
        $aStreamFeeds = array_slice($aStreamFeeds, 0, $streamLimit);
        $this->view->assign('aStreamFeeds', $aStreamFeeds);

        return $this->view->render();
    }

}